﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Micro_API.Models
{
    [Table("Collection")]
    public partial class Collection
    {
        [Key]   // def Primary Key (id)
        public long? Id { get; set; }
        public string Nom { get; set; }
        public int Userid { get; set; }

    }
}
