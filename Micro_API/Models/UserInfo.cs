﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Micro_API.Models
{
    [Table("UserInfo")]
    public partial class UserInfo
    {
        [Key]   // def Primary Key (id)
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string User_name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
