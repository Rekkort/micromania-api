﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Micro_API.Models;
using Microsoft.AspNetCore.Authorization;
using IGDB;
using GAME = IGDB.Models.Game;

namespace Micro_API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private readonly Micro_API_DBContext _context;

        public GamesController(Micro_API_DBContext context)
        {
            _context = context;
        }

        
        //============================       Recuperation liste de jeux par service IGDB =====================================

        [HttpGet]
        public IActionResult GetGameList()
        {
            _context.Games.RemoveRange(_context.Games.ToList());
            IGDBClient igdb = new IGDBClient(
                "7x0sloxk7t2h537077giuhxau3f1d6",                      //Mes clés d'identification Twitch igdb
                "zptmam9lr7xbcr7jdfykmnoybljd93"
                );
            GAME[] json = igdb.QueryAsync<GAME>(IGDBClient.Endpoints.Games, "fields id,name,rating,category,summary; where category = 0; sort rating desc; limit 10;").Result;
            foreach (GAME game in json)
            {
                Game dbgame = new Game
                {
                    Id=game.Id,                 //Association des paramètres récupérer/ champs bdd 
                    Presentation = game.Summary,
                };
                _context.Games.Add(dbgame);
            }
            _context.SaveChanges();
            return (new JsonResult(json));

        }

    }

}
