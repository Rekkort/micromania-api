﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Micro_API.Models;
using Microsoft.AspNetCore.Authorization;

namespace Micro_API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionController : ControllerBase
    {
        private readonly Micro_API_DBContext _context;

        public CollectionController(Micro_API_DBContext context)
        {
            _context = context;
        }

        //=============================      GET: api/collecs             =================================
        [HttpGet]
        public async Task<List<Collection>> GetCollection()
        {
            return await _context.Collecs.ToListAsync();
        }


        //============================          PUT: api/Collec        ===================================

        [HttpPut]
        public async Task<List<Collection>> Edit([FromBody] Collection CollecData)
        {
            _context.Collecs.Update(CollecData);
            _context.SaveChanges();
            return await _context.Collecs.ToListAsync();
        }

        // ===========================         POST: api/Collecs        ===============================
      
        [HttpPost]
        public async Task<List<Collection>> Create([FromBody] Collection CollecData)
        {
            _context.Collecs.Add(CollecData);
            _context.SaveChanges();

            return await _context.Collecs.ToListAsync(); 
        }

        // ========================          DELETE: api/Collecs/5      ===================================
        [HttpDelete]
        public async Task<List<Collection>> Delete([FromBody] Collection CollecData)
        {
            _context.Collecs.Remove(CollecData);
            _context.SaveChanges();
            return await _context.Collecs.ToListAsync();


        }

    }

}
