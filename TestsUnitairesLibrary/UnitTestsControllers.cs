using Micro_API.Controllers;
using Micro_API.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace TestsUnitairesLibrary
{
    [TestClass]
    public class UnitTestsControllers
    {
        private object game;
        private object collection;
        private Micro_API_DBContext context;
        private Task<UserInfo> token;

        //=============================================   Test of GamesController  =======================================================

        [TestMethod]
        public void TestGetIGDB()
        {
            //Arrange
            GamesController gController = new GamesController(context);

            // Act
            game = gController.GetGameList();


            // Assert
            Assert.IsNotNull(game);
        }


        //=============================================   Test of CollectionController  ===================================================
       
        [TestMethod]
        public void TestGetCollection()
        {
            //Arrange
            CollectionController cController = new CollectionController(context);

            // Act
             collection = cController.GetCollection();


            // Assert
            Assert.IsNotNull(collection);
        }

        [TestMethod]
        public void TestPutCollection()
        {
            //Arrange
            CollectionController cController = new CollectionController(context);

            // Act
            Collection Collection = new Collection { Nom = "Ma_Collection_test" };
            System.Threading.Tasks.Task<System.Collections.Generic.List<Collection>> collection = cController.Create(Collection);

            // Assert
            Assert.IsNotNull(collection);
        }

        [TestMethod]
        public void TestPostCollection()
        {
            //Arrange
            CollectionController cController = new CollectionController(context);

            // Act
            Collection Collection = new Collection {Nom= "Ma_Collection_test_V2" };
            System.Threading.Tasks.Task<System.Collections.Generic.List<Collection>> collection = cController.Edit(Collection);

            // Assert
            Assert.IsNotNull(collection);
        }



        [TestMethod]
        public void TestDeleteCollection()
        {
            //Arrange
            CollectionController cController = new CollectionController(context);

            // Act
            Collection Collection = new Collection { Nom = "Ma_Collection_test" };
            System.Threading.Tasks.Task<System.Collections.Generic.List<Collection>> collection = cController.Delete(Collection);
           

            // Assert
            Assert.IsNotNull(collection);
        }


        //=============================================   Test of TokenController  =======================================================

        [TestMethod]
        public void TestGetToken()    // Obtient Infos utilisateur
        {
            //Arrange
            TokenController tController = new TokenController(context);

            // Act
            token = tController.GetUser("email","test");


            // Assert
            Assert.IsNotNull(token);
        }

        [TestMethod]
        public async Task TestPostTokenAsync()    // Traite infos utilisateur et renvoie jwt (token)
        {
            //Arrange
            TokenController tController = new TokenController(context);

            // Act
            token = await tController.Post(_userData: Email="email",Password="test");


            // Assert
            Assert.IsNotNull(token);
        }

    }
}
